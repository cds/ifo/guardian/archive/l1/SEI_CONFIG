import time
import subprocess

from guardian import GuardState, GuardStateDecorator
from guardian import NodeManager

import cdsutils


ramp_down_time = 20 #60 for in lock switching
ramp_up_time = 20  # 120 for in lock switching
ramp_senscor_time = 60
ramp_eq_cm_mtrx = 100
corner_bsc_chambers = ['ITMX','ITMY','BS']
end_bsc_chambers = ['ETMX','ETMY']
bsc_chambers = ['ITMX','ITMY','BS','ETMX','ETMY']
#bsc_chambers = ['ETMY']
ham_chambers = ['HAM1','HAM2','HAM3','HAM4','HAM5','HAM6']
fc_ham_chambers = ['HAM7', 'HAM8']
corner_chambers = ['HAM1','HAM2','HAM3','HAM4','HAM5','HAM6','ITMX','ITMY','BS']
common_ff_bsc_chambers = bsc_chambers
quads = ['ETMX','ETMY','ITMX','ITMY']
BRS = ['ETMX','ETMY','ITMX','ITMY']
corner_brs_dof = ['X','Y']
# removed HAM1 from ham_chambers list for the vent (HAM1 is currently locked)
dofs = ['X','Y','Z']
# nominal operational state
nominal = 'ALL_ISO'
# initial request on initialization
request = 'IDLE'

nodes = NodeManager(['SEI_ITMX','SEI_ITMY','SEI_ETMX','SEI_ETMY','SEI_BS',
                     'HPI_HAM1',
                     'SEI_HAM2','SEI_HAM3','SEI_HAM4','SEI_HAM5','SEI_HAM6','SEI_HAM7','SEI_HAM8',
                     'ISI_ITMX_ST1_BLND', 'ISI_ITMY_ST1_BLND', 'ISI_BS_ST1_BLND',
                     'ISI_ETMX_ST1_BLND', 'ISI_ETMY_ST1_BLND',
		     'BRSEX_STAT','BRSEY_STAT','BRSIX_STAT','BRSIY_STAT',
])

## TIMING STUFF FOR PREP_EQ_MODE
tilt_subtraction_ramp_time = 60
eq_cm_mtrx_settling_time   = 20
short_settling_time        = 3

common_ff_fm = 'FM5' #5
nominal_bsc_ff_fm = 'FM10' #10
nominal_ham_ff_fm ='FM1' #1
senscor_gain = 1
senscor_fm   = 2
brs = {}

#############################################
# Decorator to unstall nodes.

def unstall_nodes(nodes):
    class unstall_decorator(GuardStateDecorator):
        def pre_exec(self):
            for node in nodes.get_stalled_nodes():
                # put a check that in is in done state
                if not node.NOTIFICATION:
                    log('Unstalling ' + node.name)
                    node.revive()
    return unstall_decorator

#############################################

# Checks if we are in EQ mode
#def is_any_senscor_in_eq_mode():
    #Check requested filters here, return True if even one of the senscor filters are in earthquake mode
#    for chamber in ham_chambers+bsc_chambers+fc_ham_chambers:
#        for dof in ['X','Y','Z']:
#            if check_senscor_state(chamber,dof,5):
#                notify('Sensor correction already in EQ mode. Check EQ control MEDM screen!')
#                return True
#    return False

def is_any_senscor_in_eq_mode():
    #Check requested filters here, return True if even one of the senscor filters are in earthquake mode
    for dof in ['X','Y','Z']:
        if ezca['SEI-CS_SENSCOR_' + dof + '_FADE_CUR_CHAN_MON'] == 5:
                notify('Central sensor correction already in EQ mode. Check EQ control MEDM screen!')
                return True
    for chamber in fc_ham_chambers+end_bsc_chambers:
        for dof in ['X','Y','Z']:
            if check_senscor_state(chamber,dof,5):
                notify('Sensor correction already in EQ mode. Check EQ control MEDM screen!')
                return True
    return False


# This returns the sensor correction channel name based on chamber name and dof
def sc_chan_name(chamber, dof):
    if 'HAM' in chamber:
        if 'HAM1' in chamber:
            sei = 'HPI'
            senscor = 'SENSCOR'
        else:
            sei = 'ISI'
            senscor = 'SENSCOR'
    else:
#        if 'ETMY' in chamber and 'Z' in dof:
#            sei = 'HPI'
#            senscor = 'SENSCOR'
#        else:
        sei = 'ISI'
        senscor = 'ST1_SENSCOR'
    channel_name = sei + '-' + chamber + '_' + senscor + '_' + dof
    return channel_name


def is_brs_ok(brs):
    if nodes['BRS' + brs + '_STAT'].STATE == 'READY':
        return True
    else:
        notify('BRS ' + brs + ' has a FAULT')
        return False


def is_tilt_subtraction_changing():
    for sets in brs_chambers:
        chamber = sets[0]
        dof = sets[1]
        if (int(ezca['ISI-GND_SENSCOR_' + chamber + '_STS_' + dof + '_ROTVEL_SW2R'])&4096):
            return True
        elif (int(ezca['ISI-GND_SENSCOR_' + chamber + '_STS_' + dof + '_TORQUE_SW2R'])&4096):
            return True
        elif (int(ezca['ISI-GND_SENSCOR_' + chamber + '_STS_' + dof + '_INV_GAIN'])&4096):
            return True
    return False


def get_brs_chamber_dof(brs):
    if len(brs) == 2:
        chamber = brs[0] + 'TM' + brs[-1] # write ITMX if IX
    elif len(brs) == 4:
        chamber = brs
    else:
        error('input argument is not given properly')
    dof = brs[-1]
    return chamber, dof


def is_brs_in_controls(brs):
    if brs in ['IX','IY']: # Check the corner station status
        brs_in_loop = []
        chambers = corner_chambers
        dof = get_brs_chamber_dof(brs)[1] #extract the second argument from get_brs_chamber_dof
        for chamber in chambers:
            if 1 <= ezca[sc_chan_name(chamber, dof) + '_FADE_CUR_CHAN_MON'] <= 4:
                notify('BRS ' + brs + ' is used in  ' + chamber + ' controls')
                brs_in_loop.append(chamber)
        if brs_in_loop:
            return True
        else:
            return False

    elif brs in ['EX','EY']: # Check the end station status
        [chamber, dof] = get_brs_chamber_dof(brs)
        if ezca[sc_chan_name(chamber, dof) + '_FADE_CUR_CHAN_MON'] <= 4:
            notify('BRS ' + brs + ' is used in ' + chamber + ' controls')
            return True
        else:
            return False


def is_tilt_subtraction_on(brs):
    chamber, dof = get_brs_chamber_dof(brs)
    if (ezca['ISI-GND_SENSCOR_' + chamber + '_STS_' + dof + '_ROTVEL_GAIN']==1
        and ezca['ISI-GND_SENSCOR_' + chamber + '_STS_' + dof + '_TORQUE_GAIN']==0
        and ezca['ISI-GND_SENSCOR_' + chamber + '_STS_' + dof + '_INV_GAIN']==1):
        notify(brs + ' tilt subtraction is on')
        return True
    else:
        return False


# if switch True  = brs ok  = tiltsub path ON 
#    switch False = brs off = tiltsub path OFF 
def set_sts_tilt_subtraction(brs, switch, ramp_time):
    [chamber, dof] = get_brs_chamber_dof(brs)

    # setting the ramp time
    ezca['ISI-GND_SENSCOR_' + chamber + '_STS_' + dof + '_ROTVEL_TRAMP'] = ramp_time
    ezca['ISI-GND_SENSCOR_' + chamber + '_STS_' + dof + '_TORQUE_TRAMP'] = ramp_time
    ezca['ISI-GND_SENSCOR_' + chamber + '_STS_' + dof + '_INV_TRAMP'] = ramp_time
    time.sleep(0.1)
    if switch:
        # switching from tilt subtracted to raw (switching paths)
        ezca['ISI-GND_SENSCOR_' + chamber + '_STS_' + dof + '_ROTVEL_GAIN'] = 1
        ezca['ISI-GND_SENSCOR_' + chamber + '_STS_' + dof + '_TORQUE_GAIN'] = 0
        ezca['ISI-GND_SENSCOR_' + chamber + '_STS_' + dof + '_INV_GAIN'] = 1
    else:
        ezca['ISI-GND_SENSCOR_' + chamber + '_STS_' + dof + '_ROTVEL_GAIN'] = 0
        ezca['ISI-GND_SENSCOR_' + chamber + '_STS_' + dof + '_TORQUE_GAIN'] = 1
        ezca['ISI-GND_SENSCOR_' + chamber + '_STS_' + dof + '_INV_GAIN'] = 0


def read_eq_cm_mtrx(brs):
    raw = ezca['SEI-EQ_CM_' + brs + '_MTRX_1_1']
    ss = ezca['SEI-EQ_CM_' + brs + '_MTRX_1_2']
    return raw, ss


def write_eq_cm_mtrx(brs, a, b):
    ezca['SEI-EQ_CM_' + brs + '_MTRX_TRAMP'] = ramp_eq_cm_mtrx
    time.sleep(0.2)
    ezca['SEI-EQ_CM_' + brs + '_MTRX_SETTING_1_1'] = a
    ezca['SEI-EQ_CM_' + brs + '_MTRX_SETTING_1_2'] = b
    time.sleep(0.2)
    ezca['SEI-EQ_CM_' + brs + '_MTRX_LOAD_MATRIX'] = 1
    return a, b


# Set the common mode matrix calculation
# if switch is True, brs is included, if off brs is not
def set_eq_cm_matrix(brs, switch):
    eq_cm_changed = False
    raw,ss = read_eq_cm_mtrx(brs)
    if switch: #when switch is on, we want brs included in cm
        if raw == 0 and ss == 1:
            notify(brs + ' eq cm mtrx already set correctly')
        else:
            notify('changing eq cm for sts ' + brs)
            write_eq_cm_mtrx(brs, 0, 1)
            eq_cm_changed = True
    else:
        if raw == 1 and ss == 0:
            notify(brs + ' eq cm mtrx already set correctly')
        else:
            notify('changing eq cm for sts ' + brs)
            write_eq_cm_mtrx(brs, 1, 0)
            eq_cm_changed = True
    return eq_cm_changed


def ramp_senscor(chamber, dof, ramp_time, gain):
    # set the ramp time to down the sensor correction filter
    ezca[sc_chan_name(chamber, dof) + '_MATCH_TRAMP'] = ramp_time

    # ramp down the sensor correction filter by setting the gain to 0
    ezca[sc_chan_name(chamber, dof) + '_MATCH_GAIN'] = gain

def ramp_senscor_cs(dof, ramp_time, next_chan):
    # set ramp time for senscor switching
    ezca['SEI-CS_SENSCOR_' + dof + '_MATCH_TRAMP'] = ramp_time
    time.sleep(0.1)
    # # ramp down the sensor correction filter by setting the match gain
    ezca['SEI-CS_SENSCOR_' + dof + '_MATCH_GAIN'] = gain


def switch_senscor(chamber, dof, ramp_time, next_chan):
    # set ramp time for senscor switching
    ezca[sc_chan_name(chamber, dof) + '_TRAMP'] = ramp_time
    time.sleep(0.1)
    # change senscor next chan
    ezca[sc_chan_name(chamber, dof) + '_NEXT_CHAN'] = next_chan

def switch_senscor_cs(dof, ramp_time, next_chan):
    # set ramp time for senscor switching
    ezca['SEI-CS_SENSCOR_' + dof + '_TRAMP'] = ramp_time
    time.sleep(0.1)
    # change senscor next chan
    ezca['SEI-CS_SENSCOR_' + dof + '_NEXT_CHAN'] = next_chan


def check_senscor_done(chamber, dof, next_chan):
    a = ezca[sc_chan_name(chamber, dof) + '_FADE_CUR_CHAN_MON']
    b = ezca[sc_chan_name(chamber, dof) + '_NEXT_CHAN']
    if a != b:
        notify('waiting to transition ' + chamber + ' ' + dof)
        return False
    else:
        notify('transition complete')
        return True

def check_senscor_cs_done(dof, next_chan):
    a = ezca['SEI-CS_SENSCOR_' + dof + '_FADE_CUR_CHAN_MON']
    b = ezca['SEI-CS_SENSCOR_' + dof + '_NEXT_CHAN']
    if a != b:
        notify('waiting to transition seiproc CS sencor ' + dof)
        return False
    else:
        notify('transition complete')
        return True


def check_senscor_state(chamber,dof,next_chan):
    a = ezca[sc_chan_name(chamber, dof) + '_FADE_CUR_CHAN_MON']
    if a != next_chan:
        return False
    else:
        return True


def senscor_from_brsguard(chamber, dof):
    if dof in 'Z':
        sc_fm = 1
    else:
        if chamber in corner_chambers:
        #check BRS guardian status      
            if is_brs_ok('IX') and is_brs_ok('IY'):
                sc_fm = 1
            else:
                sc_fm = 8
        elif chamber in 'ETMX':
            if 'X' in dof:
                if is_brs_ok('EX'):
                    sc_fm = 1
                else:
                    sc_fm = 8
            else:
                sc_fm = 1
        elif chamber in 'ETMY':
            if 'Y' in dof:
                if is_brs_ok('EY'):
                    sc_fm = 1
                else:
                    sc_fm = 8
            else:
                sc_fm = 1
    return sc_fm

#########################################################
# GUARDIAN CLASS DEFINITION STARTS HERE
#########################################################

class INIT(GuardState):
    def main(self):
        return True


class IDLE(GuardState):
    goto = True
    index = 1

    def main(self):
        return True


#TODO : add PREP_GO_BACK state to run before launching EARHTQUAKE_OFF mode
class PREP_EQ_MODE(GuardState):
    request = True
    #goto = True

    def main(self):
        self.status = True #Flagging is state is done or not

        #JCB 2022/06/07 movd eq_cm_changed definition so it is always defined
        eq_cm_changed = []
        # SETTING THE CM SIGNAL
        #checking if we are already in EQ mode even on one station.Do not change CM matrix
        if is_any_senscor_in_eq_mode():
            notify('Cannot change CM matrix. Please check.')
            self.status = False
        else:   # Setting the CM matrix according to BRS status
            #eq_cm_changed = []
            for brs in ['EY', 'EX', 'IY', 'IX']:
                eq_cm_changed.append(set_eq_cm_matrix(brs,is_brs_ok(brs)))
                if True in eq_cm_changed:
                    self.timer['wait'] = eq_cm_mtrx_settling_time + ramp_eq_cm_mtrx
                else:
                    self.timer['wait'] = short_settling_time

        # SETTING THE TILT SUBTRACTION
        #check we are not using tilt subtraction signal in loop if changing it
        for brs, switchstate in zip(['EY', 'EX', 'IY', 'IX'],['SWITCH_ENDY_BRS_OFF', 'SWITCH_ENDX_BRS_OFF', 'SWITCH_CORNER_BRS_OFF', 'SWITCH_CORNER_BRS_OFF']):
        # two cases which result in the same outcome: 1) BRS is ok and tilt sub is OFF. 2)BRS is not ok and tilt sub is ON
            if (is_brs_ok(brs) and not is_tilt_subtraction_on(brs)) or (not is_brs_ok(brs) and is_tilt_subtraction_on(brs)):
                if not is_brs_in_controls(brs): #check senscor to see if BRS is used
                    set_sts_tilt_subtraction(brs,is_brs_ok(brs),tilt_subtraction_ramp_time)
                    log('changing tilt subtraction for ' + brs)
                    if not (True in eq_cm_changed):
                        self.timer['wait'] = tilt_subtraction_ramp_time
                else:
                    notify('BRS' + brs + ' is used in loop, SWITCHING_BRS' + brs + '_OFF FIRST')
                    return switchstate
		    #self.status = False
        #return self.status

    def run(self):
        if self.status:
            if self.timer['wait']:
                notify('EQ mode is READY')
                return True
            else:
                notify('signals settling down')
        else:
            notify('EQ mode NOT READY')
#        return self.status #IMPORTANT FOR SEI_EQ GUARDIAN


class STEPDOWN_TILT_SUB_FROM_EQ_MODE(GuardState):
    request = True
    # goto = True

    def main(self):
        # SETTING THE TILT SUBTRACTION AFTER EQ
        #check we are not using tilt subtraction signal in loop if changing it
        for brs in ['EY', 'EX', 'IY', 'IX']:
            if not is_brs_in_controls(brs): #check senscor to see if BRS is used
                set_sts_tilt_subtraction(brs, True, tilt_subtraction_ramp_time)
                log('changing tilt subtraction for ' + brs)
            else:
                notify('Cannot transition. BRS' + brs + ' is used in loop, Select SWITCH_BRS' + brs + '_OFF')


#class RAMP_DOWN_CORNER_SENSCOR(GuardState):#-----> OLD version with individual stations
#    request = True
#    goto = True

#    def main(self):
#        for chamber in corner_bsc_chambers+ham_chambers:
#            for dof in dofs:
#                ramp_senscor(chamber, dof, ramp_down_time, 0)
#                self.timer['foo'] = ramp_down_time

#    def run(self):
#        return self.timer['foo']

class RAMP_DOWN_CORNER_SENSCOR(GuardState): # Switch corner SC OFF with new central sc
    request = True
    goto = True

    def main(self):
        for dof in dofs:
            switch_senscor_cs(dof, ramp_down_time, 0)
            self.timer['foo'] = ramp_down_time

    def run(self):
        return self.timer['foo']


#class RAMP_UP_CORNER_SENSCOR(GuardState): #-----> OLD version with individual stations
#    request = True
#    goto = True

#    def main(self):
#        for chamber in corner_bsc_chambers+ham_chambers:
#            for dof in dofs:
#                ramp_senscor(chamber, dof, ramp_up_time, 1)
#                self.timer['foo'] = ramp_up_time

#    def run(self):
#        return self.timer['foo']

class RAMP_UP_CORNER_SENSCOR(GuardState): # Switch corner SC ON with new central sc
    request = True
    goto = True

    def main(self):
        sc_fm = []
        self.x_corr = senscor_from_brsguard('ITMX','X')
        self.y_corr = senscor_from_brsguard('ITMY','Y')
        switch_senscor_cs('X',ramp_up_time, self.x_corr)
        switch_senscor_cs('Y',ramp_up_time, self.y_corr)
        switch_senscor_cs('Z',ramp_up_time, 1) #Z does not have tilt substraction, so 1 is the same as 8

    def run(self):
        ret = True
        if not (check_senscor_cs_done('X',self.x_corr) and check_senscor_cs_done('Y',self.y_corr) and check_senscor_cs_done('Z',1)):
           ret = False                


class SWITCH_BSCs_TO_BLEND_MH_105(GuardState):
    goto = True
    request = True

    def main(self):
        for chamber in bsc_chambers:
            for dof in dofs:
                # switch the blend filtering to the nominal ff configuration
                nodes['ISI_{}_ST1_BLND'.format(chamber)] = 'CONFIG_MH_105'
                self.timer['foo'] = 60

    def run(self):
        return self.timer['foo']


class SWITCH_BSCs_TO_BLEND_45MHZ(GuardState):
    goto = True
    request = True

    def main(self):
        for chamber in bsc_chambers:
            for dof in dofs:
                # switch the blend filtering to the nominal ff configuration
                nodes['ISI_{}_ST1_BLND'.format(chamber)] = 'CONFIG_45MHZ'
                self.timer['foo'] = 120

    def run(self):
        return self.timer['foo']


class EARTHQUAKE_OFF(GuardState):
    request = True
    index = 10

    def main(self):

        time.sleep(2)

        return True


#class SWITCH_IFO_SENSCOR_TO_NOMINAL(GuardState): # Switch IFO senscor to Nominal mode
#    request = True
#    goto = True
#    index = 9

#    def main(self):
#        sc_fm = []
#        for chamber in bsc_chambers+ham_chambers:
#            for dof in ['X', 'Y', 'Z']:
#                sc_fm = senscor_from_brsguard(chamber, dof)
#                switch_senscor(chamber, dof, ramp_senscor_time, sc_fm) # used to be 1

#    def run(self):
#        ret = True
#        for chamber in bsc_chambers+ham_chambers:
#           for dof in ['X', 'Y', 'Z']:
#                sc_fm = senscor_from_brsguard(chamber, dof)
#                if not check_senscor_done(chamber, dof, sc_fm):
#                    ret = False
#        return ret

class SWITCH_IFO_SENSCOR_TO_NOMINAL(GuardState): # Switch IFO senscor to Nominal mode
    request = True
    goto = True
    index = 9

    def main(self):
        sc_fm = []
        for chamber in end_bsc_chambers:
            for dof in ['X', 'Y', 'Z']:
                sc_fm = senscor_from_brsguard(chamber, dof)
                switch_senscor(chamber, dof, ramp_senscor_time, sc_fm) # used to be 1
# for central
        self.x_corr = senscor_from_brsguard('ITMX','X')
        self.y_corr = senscor_from_brsguard('ITMY','Y')
        switch_senscor_cs('X',ramp_senscor_time, self.x_corr)
        switch_senscor_cs('Y',ramp_senscor_time, self.y_corr)
        switch_senscor_cs('Z',ramp_senscor_time, 1) #Z does not have tilt substraction, so 1 is the same as 8.
# for FC_HAMs
        for chamber in fc_ham_chambers:
            for dof in ['X', 'Y', 'Z']:
                switch_senscor(chamber, dof, ramp_senscor_time, 8) # We do not use BRS so 8

    def run(self):
        ret = True
        for chamber in end_bsc_chambers:
            for dof in ['X', 'Y', 'Z']:
                sc_fm = senscor_from_brsguard(chamber, dof)
                if not check_senscor_done(chamber, dof, sc_fm):
                    ret = False
        for chamber in fc_ham_chambers:
            for dof in ['X', 'Y', 'Z']:
                if not check_senscor_done(chamber, dof, 8):
                    ret = False
        if not (check_senscor_cs_done('X',self.x_corr) and check_senscor_cs_done('Y',self.y_corr) and check_senscor_cs_done('Z',1)):
            ret = False
                
        return ret

#What is this function for??? We don't use it for anything.
#class SWITCH_IFO_SENSCOR_FM(GuardState): # Switch IFO senscor to whatever SC
#    request = True
#    goto = True
#    index = 8

#    def main(self):
#        for chamber in bsc_chambers+ham_chambers:
#            for dof in ['X', 'Y']:
#                switch_senscor(chamber, dof, ramp_senscor_time, senscor_fm)

#    def run(self):
#        ret = True
#        for chamber in bsc_chambers+ham_chambers:
#            for dof in ['X', 'Y', 'Z']:
#                if not check_senscor_done(chamber, dof, senscor_fm):
#                    ret = False
#        return ret


class EARTHQUAKE_ON(GuardState):
    request = True
    index = 15

    def main(self):
        return True


#class SWITCH_IFO_SENSCOR_TO_EQ_DM(GuardState): # Switch IFO senscor to EQ DIFF mode
#    request = True
#    index = 14

#    def main(self):
#        for chamber in bsc_chambers+ham_chambers:
#            for dof in ['X', 'Y', 'Z']:
#                switch_senscor(chamber, dof, ramp_senscor_time, 5)

#    def run(self):
#        ret = True
#        for chamber in bsc_chambers+ham_chambers:
#            for dof in ['X', 'Y', 'Z']:
#                if not check_senscor_done(chamber, dof, 5):
#                    ret = False
#        return ret

class SWITCH_IFO_SENSCOR_TO_EQ_MODE(GuardState): # Switch IFO senscor to EQ DIFF mode
    request = True
    index = 18

    def main(self):
        for dof in ['X', 'Y', 'Z']:
            switch_senscor_cs(dof, ramp_senscor_time, 5) #This is using the new SEIPROC corner station sensor correction
        for chamber in fc_ham_chambers+end_bsc_chambers:
            for dof in ['X', 'Y', 'Z']:
                switch_senscor(chamber, dof, ramp_senscor_time, 5)

    def run(self):
        ret = True
        for dof in ['X', 'Y', 'Z']:
            for chamber in fc_ham_chambers+end_bsc_chambers:
                if not check_senscor_done(chamber, dof, 5):
                    ret = False
            if not check_senscor_cs_done(dof, 5):
                ret = False #This is using the new SEIPROC corner station sensor correction
        return ret



class CORNER_BRS_OFF(GuardState):
    request = True
    index = 55

    def main(self):
        return True


#class SWITCH_CORNER_BRS_OFF(GuardState): # Switch senscor from super STS smoothly -----> OLD version with individual stations
#    request = True
#    goto = True
#    index = 54

#    def main(self):
#        for chamber in corner_bsc_chambers+ham_chambers:
#            for dof in corner_brs_dof:
#                switch_senscor(chamber, dof, ramp_senscor_time, 8)

#    def run(self):
#        ret = True
#        for chamber in corner_bsc_chambers+ham_chambers:
#            for dof in corner_brs_dof:
#                if not check_senscor_done(chamber, dof, 8):
#                    ret = False
#        return ret

class SWITCH_CORNER_SENSCOR_IND_TO_CENTRAL(GuardState): # Switch senscor from individual stations to central SEIPROC
    request = True
    goto = True
    index = 112

    def main(self):
        for chamber in corner_bsc_chambers+ham_chambers:
            for dof in corner_brs_dof:
                switch_senscor(chamber, dof, ramp_up_time, 10)
            switch_senscor(chamber,'Z',ramp_up_time, 10)

    def run(self):
        ret = True
        for chamber in corner_bsc_chambers+ham_chambers:
            for dof in corner_brs_dof:
                if not check_senscor_done(chamber, dof, 10):
                    ret = False
        return ret

#class SWITCH_SENSCOR_SEIPROC_TO_IND(GuardState): # Switch IFO senscor to Nominal mode -----> This should be avoided if possible. Turn a single SC station by going to the individual MEDM screen and turn it manually
#    request = True
#    goto = True
#    index = 99

#    def main(self):
#        sc_fm = []
#        for chamber in bsc_chambers+ham_chambers:
#            for dof in ['X', 'Y']:
#                sc_fm = senscor_from_brsguard(chamber, dof)
#                switch_senscor(chamber, dof, ramp_down_time, sc_fm)
#            switch_senscor(chamber,'Z',ramp_down_time, 1)

#    def run(self):
#        ret = True
#        for chamber in bsc_chambers+ham_chambers:
#           for dof in ['X', 'Y', 'Z']:
#                sc_fm = senscor_from_brsguard(chamber, dof)
#                if not check_senscor_done(chamber, dof, sc_fm):
#                    ret = False
#        return ret

class SWITCH_CORNER_BRS_OFF(GuardState): # Switch corner BRS out of loop with new central sc
    request = True
    goto = True
    index = 54

    def main(self):
        for dof in corner_brs_dof:
            switch_senscor_cs(dof, ramp_senscor_time, 8)
        self.timer['foo'] = ramp_senscor_time

    def run(self):
        ret = True
        for dof in corner_brs_dof:
            if not check_senscor_cs_done(dof, 8):
                ret = False
        return ret


class CORNER_BRS_ON(GuardState):
    request = True
    index = 50

    def main(self):
        return True


#class SWITCH_CORNER_BRS_ON(GuardState): # Switch senscor to super STS smoothly -----> OLD version with individual stations
#    request = True
#    goto = True
#    index = 49

#    def main(self):
#        for chamber in corner_bsc_chambers+ham_chambers:
#            for dof in corner_brs_dof:
#                switch_senscor(chamber, dof, ramp_senscor_time, 1)
#        self.timer['foo'] = ramp_senscor_time

#    def run(self):
#        ret = True
#        for chamber in corner_bsc_chambers+ham_chambers:
#            for dof in corner_brs_dof:
#                if not check_senscor_done(chamber, dof, 1):
#                    ret = False
#        return ret

class SWITCH_CORNER_BRS_ON(GuardState): # Switch corner BRS in loop with new central sc
    request = True
    goto = True
    index = 49

    def main(self):
        for dof in corner_brs_dof:
            switch_senscor_cs(dof, ramp_senscor_time, 1)
        self.timer['foo'] = ramp_senscor_time

    def run(self):
        ret = True
        for dof in corner_brs_dof:
            if not check_senscor_cs_done(dof, 1):
                ret = False
        return ret

class ENDX_BRS_ON(GuardState):
    request = True
    index = 45

    def main(self):
        return True


class SWITCH_ENDX_BRS_ON(GuardState): # Switch ENDX senscor to super STS smoothly
    request = True
    goto = True
    index = 44

    def main(self):
        ezca['ISI-ETMX_ST1_SENSCOR_X_TRAMP'] = ramp_senscor_time
        ezca['ISI-ETMX_ST1_SENSCOR_X_NEXT_CHAN'] = 1
        self.timer['foo'] = ramp_senscor_time

    def run(self):
        if not self.timer['foo']:
            notify('transitioning...')
            return
        return True


class ENDX_BRS_OFF(GuardState):
    request = True
    index = 40

    def main(self):
        return True


class SWITCH_ENDX_BRS_OFF(GuardState): # Switch ENDY senscor to super STS smoothly
    request = True
    goto = True
    index = 39

    def main(self):
        ezca['ISI-ETMX_ST1_SENSCOR_X_TRAMP'] = 60
        ezca['ISI-ETMX_ST1_SENSCOR_X_NEXT_CHAN'] = 8
        self.timer['foo'] = ramp_senscor_time

    def run(self):
        if not self.timer['foo']:
            notify('transitioning...')
            return
        return True


class ENDY_BRS_ON(GuardState):
    request = True
    index = 35

    def main(self):
        return True


class SWITCH_ENDY_BRS_ON(GuardState): # Switch ENDY senscor to super STS smoothly
    request = True
    goto = True
    index = 34

    def main(self):
        ezca['ISI-ETMY_ST1_SENSCOR_Y_TRAMP'] = 60
        ezca['ISI-ETMY_ST1_SENSCOR_Y_NEXT_CHAN'] = 1
        self.timer['foo'] = ramp_senscor_time

    def run(self):
        if not self.timer['foo']:
            notify('transitioning...')
            return
        return True


class ENDY_BRS_OFF(GuardState):
    request = True
    index = 30

    def main(self):
        return True


class SWITCH_ENDY_BRS_OFF(GuardState): # Switch ENDY senscor to super STS smoothly
    request = True
    goto = True
    index = 29

    def main(self):
        ezca['ISI-ETMY_ST1_SENSCOR_Y_TRAMP'] = 60
        ezca['ISI-ETMY_ST1_SENSCOR_Y_NEXT_CHAN'] = 8
        self.timer['foo'] = ramp_senscor_time

    def run(self):
        if not self.timer['foo']:
            notify('transitioning...')
            return
        return True


class CHANGE_SENSCOR_GAIN(GuardState):
    request = True
    goto = True

    def main(self):
        for chamber in ham_chambers+bsc_chambers:
            for dof in ['X', 'Y']:
                ramp_time = 5
                ramp_senscor(chamber, dof, ramp_time, senscor_gain)

    def run(self):
        return True


class ALL_OFFLINE(GuardState):
#    goto = True
    request = True
    index = 80

    def main(self):
        for chamber in bsc_chambers:   
            nodes['SEI_' + chamber] = 'OFFLINE'
        for chamber in ham_chambers+fc_ham_chambers:
            if 'HAM1' in chamber:
                nodes['HPI_' + chamber] = 'READY'                            
            else:
                nodes['SEI_' + chamber] = 'OFFLINE'

    def run(self):
        if nodes.arrived:
            return True


class CORNER_DAMPED(GuardState):
#    goto = True
    request = True
    index = 65

    def main(self):
        for chamber in corner_bsc_chambers:
            nodes['SEI_' + chamber] = 'DAMPED'
        for chamber in ham_chambers[1:]:
                nodes['SEI_' + chamber] = 'DAMPED'

    def run(self):
        if nodes.arrived:
            return True


class CORNER_ISO(GuardState):
    request = True
    index = 70

    def main(self):
        for chamber in corner_bsc_chambers:
            if 'BS' in chamber:
                nodes['SEI_' + chamber] = 'ISOLATED_DAMPED'
            else:
                nodes['SEI_' + chamber] = 'FULLY_ISOLATED'
        for chamber in ham_chambers:
            if 'HAM1' in chamber:
                nodes['HPI_' + chamber] = 'ROBUST_ISOLATED'
            else:
                nodes['SEI_' + chamber] = 'ISOLATED'

    def run(self):
        if nodes.arrived:
            return True


class CORNER_OFFLINE(GuardState):
    request = True
    index = 60

    def main(self):
        for chamber in corner_bsc_chambers:
            nodes['SEI_' + chamber] = 'OFFLINE'
        for chamber in ham_chambers:
            if 'HAM1' in chamber:
                nodes['HPI_' + chamber] = 'READY'
            else:
                nodes['SEI_' + chamber] = 'OFFLINE'

    def run(self):
        if nodes.arrived:
            return True


class ALL_DAMPED(GuardState):
#    goto = True
    request = True
    index = 90

    def main(self):
        for chamber in bsc_chambers:
            nodes['SEI_' + chamber] = 'DAMPED'
        for chamber in ham_chambers[1:]+fc_ham_chambers:
            nodes['SEI_' + chamber] = 'DAMPED'

    def run(self):
        if nodes.arrived:
            return True

class ALL_DAMPED_HEPI_OFFLINE(GuardState):
#    goto = True
    request = True
    index = 91

    def main(self):
        for chamber in bsc_chambers:
            nodes['SEI_' + chamber] = 'ISI_DAMPED_HEPI_OFFLINE'
        for chamber in ham_chambers+fc_ham_chambers:
            if 'HAM1' in chamber:
                nodes['HPI_' + chamber] = 'READY'
            else:
                nodes['SEI_' + chamber] = 'DAMPED'
    def run(self):
        if nodes.arrived:
            return True

class TURN_ON_ISIFF(GuardState):
    goto = True
    request = True

    def main(self):
        for quad in quads:
            ezca['SUS-' + quad + '_M0_ISIFF_L2P_TRAMP'] = 10
            ezca['SUS-' + quad + '_M0_ISIFF_L2P_GAIN'] = -1

    def run(self):
        return True


class TURN_OFF_ISIFF(GuardState):
    goto = True
    request = True

    def main(self):
        for quad in quads:
            ezca['SUS-' + quad + '_M0_ISIFF_L2P_TRAMP'] = 10
            ezca['SUS-' + quad + '_M0_ISIFF_L2P_GAIN'] = 0

    def run(self):
        return True


class ALL_ISO(GuardState):
#    goto = True
    request = True
    index = 100

    def main(self):
        if ezca['GRD-ISC_LOCK_STATE_N'] > 855:
            return True
        else:
            for chamber in bsc_chambers:
                if 'BS' in chamber:
                    nodes['SEI_' + chamber] = 'ISOLATED_DAMPED'
                else:
                    nodes['SEI_' + chamber] = 'FULLY_ISOLATED'
            for chamber in ham_chambers+fc_ham_chambers:
                if 'HAM1' in chamber:
                    nodes['HPI_' + chamber] = 'ROBUST_ISOLATED'
                else:
                    nodes['SEI_' + chamber] = 'ISOLATED'

    # FIXME: Give this a try sometime Arnaud! Wish I could have tried it before I left - TJ
    #@unstall_nodes
    def run(self):
        if ezca['GRD-ISC_LOCK_STATE_N'] > 855:
            return True
        else:
            if nodes.arrived:
                return True

class SET_CORNER_SENSOR_GAINS_LOW(GuardState):
    goto = True
    request = True
    def main(self):

            # Switch all intertial sensors to low gain
        geophoneDOFs = ['H1','H2','H3','V1','V2','V3'] # geophones = L4Cs and GS13s
        t240DOFs =  ['X1','X2','X3'] # X DOF FM10 controls gain switch for all DOFs

        for thisChamber in ham_chambers[1:6]:
            if any(L4Cs in thisChamber for L4Cs in ['4','5']):
                for thisL4CDOF in geophoneDOFs:
                    ezca.get_LIGOFilter('ISI-{}_L4CINF_{}'.format(thisChamber,thisL4CDOF)).turn_on('FM4') #Already ON nominally
            for thisGS13DOF in geophoneDOFs:
                ezca.get_LIGOFilter('ISI-{}_GS13INF_{}'.format(thisChamber,thisGS13DOF)).turn_on('FM4') #Already ON nominally
                ezca.get_LIGOFilter('ISI-{}_GS13INF_{}'.format(thisChamber,thisGS13DOF)).turn_off('FM5') #This is ON nominally

        for thisChamber in corner_bsc_chambers:
            for thisT240DOF in t240DOFs:
                ezca.get_LIGOFilter('ISI-{}_ST1_T240INF_{}'.format(thisChamber,thisT240DOF)).turn_off('FM10') #This is ON nominally
            for thisL4CDOF in geophoneDOFs:
                ezca.get_LIGOFilter('ISI-{}_ST1_L4CINF_{}'.format(thisChamber,thisL4CDOF)).turn_on('FM4') #This is OFF nominally
            for thisGS13DOF in geophoneDOFs:
                ezca.get_LIGOFilter('ISI-{}_ST2_GS13INF_{}'.format(thisChamber,thisGS13DOF)).turn_on('FM4') #Already ON nominally
                ezca.get_LIGOFilter('ISI-{}_ST2_GS13INF_{}'.format(thisChamber,thisGS13DOF)).turn_off('FM5') #This is ON nominally

    def run(self):
        return True

class SET_CORNER_SENSOR_GAINS_NOMINAL(GuardState):
    goto = True
    request = True
    def main(self):

            # Switch all intertial sensors to low gain
        geophoneDOFs = ['H1','H2','H3','V1','V2','V3'] # geophones = L4Cs and GS13s
        t240DOFs =  ['X1','X2','X3'] # X DOF FM10 controls gain switch for all DOFs

        for thisChamber in ham_chambers[1:6]:
            if any(L4Cs in thisChamber for L4Cs in ['4','5']):
                for thisL4CDOF in geophoneDOFs:
                    ezca.get_LIGOFilter('ISI-{}_L4CINF_{}'.format(thisChamber,thisL4CDOF)).turn_on('FM4') #Already ON nominally
            for thisGS13DOF in geophoneDOFs:
                ezca.get_LIGOFilter('ISI-{}_GS13INF_{}'.format(thisChamber,thisGS13DOF)).turn_on('FM4') #Already ON nominally
                ezca.get_LIGOFilter('ISI-{}_GS13INF_{}'.format(thisChamber,thisGS13DOF)).turn_on('FM5') #This is ON nominally

        for thisChamber in corner_bsc_chambers:
            for thisT240DOF in t240DOFs:
                ezca.get_LIGOFilter('ISI-{}_ST1_T240INF_{}'.format(thisChamber,thisT240DOF)).turn_on('FM10') #This is ON nominally
            for thisL4CDOF in geophoneDOFs:
                ezca.get_LIGOFilter('ISI-{}_ST1_L4CINF_{}'.format(thisChamber,thisL4CDOF)).turn_off('FM4') #This is OFF nominally
            for thisGS13DOF in geophoneDOFs:
                ezca.get_LIGOFilter('ISI-{}_ST2_GS13INF_{}'.format(thisChamber,thisGS13DOF)).turn_on('FM4') #Already ON nominally
                ezca.get_LIGOFilter('ISI-{}_ST2_GS13INF_{}'.format(thisChamber,thisGS13DOF)).turn_on('FM5') #This is ON nominally

    def run(self):
        return True

class SET_ACT_WD_TRESHOLD_HIGH(GuardState):
    goto = True
    request = True
    def main(self):
           # Increase allowable count of actuator saturations to max
        maxActSatCount = 8192
        for thisChamber in ham_chambers[1:6]+fc_ham_chambers:
            ezca['ISI-{}_WD_ACT_SET_SAFE_COUNT'.format(thisChamber)] = maxActSatCount
        for thisChamber in bsc_chambers:
            ezca['ISI-{}_ST1_WD_ACT_SET_SAFE_COUNT'.format(thisChamber)] = maxActSatCount
            time.sleep(0.1)
            ezca['ISI-{}_ST2_WD_ACT_SET_SAFE_COUNT'.format(thisChamber)] = maxActSatCount
            time.sleep(0.1)

    def run(self):
        return True

class SET_ACT_WD_TRESHOLD_NOMINAL(GuardState):
    goto = True
    request = True
    def main(self):
        # Return allowable count of actuator saturations to max
        nomActSatCount = 100
        for thisChamber in ham_chambers[1:6]:
            # HAM6 is special
            if thisChamber == 'HAM6':
                continue
            else:
                ezca['ISI-{}_WD_ACT_SET_SAFE_COUNT'.format(thisChamber)] = nomActSatCount
        for thisChamber in bsc_chambers:
            ezca['ISI-{}_ST1_WD_ACT_SET_SAFE_COUNT'.format(thisChamber)] = nomActSatCount
            ezca['ISI-{}_ST2_WD_ACT_SET_SAFE_COUNT'.format(thisChamber)] = nomActSatCount

    def run(self):
        return True

class RESET_ALL_WD(GuardState):
    request = True
    index = 20

    def main(self):
        for chamber in bsc_chambers:
            ezca['ISI-' + chamber + '_WD_RSET'] = 1
            ezca['HPI-' + chamber + '_WD_RSET'] = 1
            ezca['IOP-SEI_' + chamber + '_DACKILL_RESET'] = 1
            ezca['ISI-' + chamber + '_MASTERSWITCH'] = 1
        for chamber in ham_chambers+fc_ham_chambers:
            if chamber in 'HAM1':
                ezca['HPI-' + chamber + '_WD_RSET'] = 1
            elif chamber in ['HAM7', 'HAM8']:
                ezca['ISI-' + chamber + '_WD_RSET'] = 1
                ezca['IOP-SEI_' + chamber + '_DACKILL_RESET'] = 1
                ezca['ISI-' + chamber + '_MASTERSWITCH'] = 1    
            else:
                ezca['ISI-' + chamber + '_WD_RSET'] = 1
                ezca['HPI-' + chamber + '_WD_RSET'] = 1
                ezca['IOP-SEI_' + chamber + '_DACKILL_RESET'] = 1
                ezca['ISI-' + chamber + '_MASTERSWITCH'] = 1

    def run(self):
        if nodes.arrived:
            return True

class TURN_ON_ST2_TILT(GuardState):
    request = True
    goto = True

    def main(self):
        tilt_dof = ['RX', 'RY']
        for chamber in bsc_chambers:
            for tilts in tilt_dof:
                ezca.switch('ISI-' + chamber + '_ST2_ISO_' + tilts, 'INPUT', 'OUTPUT', 'FM2', 'ON')
                ezca['ISI-' + chamber + '_ST2_ISO_' + tilts + '_TRAMP'] = 10
        for chamber in bsc_chambers:
            for tilts in tilt_dof:
                ezca['ISI-' + chamber + '_ST2_ISO_' + tilts + '_GAIN'] = 0.01
        time.sleep(10)
        for chamber in bsc_chambers:
            for tilts in tilt_dof:
                ezca['ISI-' + chamber + '_ST2_ISO_' + tilts + '_GAIN'] = 1
        time.sleep(10)
        for chamber in bsc_chambers:
            for tilts in tilt_dof:
                ezca.switch('ISI-' + chamber + '_ST2_ISO_' + tilts, 'FM9', 'ON')
            
    def run(self):
        return True

class TURN_OFF_ST2_TILT(GuardState):
    request = True
    goto = True

    def main(self):
        tilt_dof = ['RX', 'RY']
        for chamber in bsc_chambers:
            for tilts in tilt_dof:
                ezca.switch('ISI-' + chamber + '_ST2_ISO_' + tilts, 'FM9', 'OFF')
        time.sleep(5)
        for chamber in bsc_chambers:
            for tilts in tilt_dof:
                ezca['ISI-' + chamber + '_ST2_ISO_' + tilts + '_TRAMP'] = 10
                ezca['ISI-' + chamber + '_ST2_ISO_' + tilts + '_GAIN'] = 0.01
        time.sleep(10)
        for chamber in bsc_chambers:
            for tilts in tilt_dof:
                ezca['ISI-' + chamber + '_ST2_ISO_' + tilts + '_GAIN'] = 0
        time.sleep(10)
        for chamber in bsc_chambers:
            for tilts in tilt_dof:
                ezca['ISI-' + chamber + '_ST2_ISO_' + tilts + '_TRAMP'] = 0
                ezca.switch('ISI-' + chamber + '_ST2_ISO_' + tilts, 'INPUT', 'OUTPUT', 'FM2', 'OFF')

    def run(self):
        return True


edges = [
    ('INIT', 'IDLE'),
    ('IDLE', 'ALL_OFFLINE'),
    ('IDLE', 'ALL_DAMPED'),
    ('IDLE', 'ALL_DAMPED_HEPI_OFFLINE'),
    ('IDLE', 'ALL_ISO'),
    ('IDLE', 'CORNER_OFFLINE'),
    ('IDLE', 'CORNER_DAMPED'),
    ('IDLE', 'CORNER_ISO'),
    ('IDLE', 'RESET_ALL_WD'),
    ('SWITCH_CORNER_SENSCOR_IND_TO_CENTRAL', 'CORNER_BRS_OFF'),
    ('SWITCH_CORNER_BRS_OFF','SWITCH_CORNER_SENSCOR_IND_TO_CENTRAL'),
    ('SWITCH_CORNER_SENSCOR_IND_TO_CENTRAL', 'CORNER_BRS_ON'),
    ('SWITCH_CORNER_BRS_ON','SWITCH_CORNER_SENSCOR_IND_TO_CENTRAL'),
    ('SWITCH_ENDX_BRS_OFF', 'ENDX_BRS_OFF'),
    ('SWITCH_ENDX_BRS_ON', 'ENDX_BRS_ON'),
    ('SWITCH_ENDY_BRS_OFF', 'ENDY_BRS_OFF'),
    ('SWITCH_ENDY_BRS_ON', 'ENDY_BRS_ON'),
#Flow for EQ_On
    ('SWITCH_IFO_SENSCOR_TO_EQ_MODE', 'EARTHQUAKE_ON'),
    ('PREP_EQ_MODE', 'SWITCH_IFO_SENSCOR_TO_EQ_MODE'),
    ('SWITCH_CORNER_SENSCOR_IND_TO_CENTRAL','PREP_EQ_MODE'),
#Flow for EQ_Off
    ('SWITCH_IFO_SENSCOR_TO_NOMINAL', 'EARTHQUAKE_OFF'),
    #('STEPDOWN_TILT_SUB_FROM_EQ_MODE', 'EARTHQUAKE_OFF'),
    #('SWITCH_IFO_SENSCOR_TO_NOMINAL', 'STEPDOWN_TILT_SUB_FROM_EQ_MODE'),
]
